/*
Godate is a libary to make working with dates easier.
All functions work with the time package.
The only reason to use this package is for simplicity.
All dates assume UTC.
*/

package godate

import (
	"fmt"
	"strings"
	"time"
)

type Year int
type Month int
type Day int

type Date struct {
	// Self explanatory
	Day   Day
	Month Month
	Year  Year

	// A golang time representation
	GoTime time.Time
}

func NewDate(year, month, day int) Date {
	var d Date
	d.Year = Year(year)
	d.Month = Month(month)
	d.Day = Day(day)

	d.GoTime = time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC)
	return d
}

// Follows ISO 8601 standard
func (d *Date) String() string {
	return fmt.Sprintf("%d-%02d-%02d", d.Year, d.Month, d.Day)
}

func NewDateFromTime(t time.Time) Date {
	return NewDate(t.Year(), int(t.Month()), t.Day())
}

func Since(d Date) (days int) {
	return int(time.Since(d.GoTime).Hours() * 24)
}

func (m *Month) String() string {
	return m.LongName()
}

var Months =[12]string{
"January",
"Feburary",
"March",
"April",
"May",
"June",
"July",
"August",
"September",
"October",
"November",
"December",
}

func (m *Month) LongName() string {
	return Months[int(*m) -1]
}

// Returns the currect month based on a case-insensitive string
// Works with full name of month or 3 letter representation
// i.e. jan or January
func createMonth(s string) Month {
	switch strings.ToLower(s) {
	case "jan":
		fallthrough
	case "january":
		return Month(1)
	case "feb":
		fallthrough
	case "febuary":
		return Month(2)
	case "mar":
		fallthrough
	case "march":
		return Month(3)
	case "apr":
		fallthrough
	case "april":
		return Month(4)
	case "may":
		return Month(5)
	case "jun":
		fallthrough
	case "june":
		return Month(6)
	case "jul":
		fallthrough
	case "july":
		return Month(7)
	case "aug":
		fallthrough
	case "august":
		return Month(8)
	case "sep":
		fallthrough
	case "september":
		return Month(9)
	case "oct":
		fallthrough
	case "october":
		return Month(10)
	case "nov":
		fallthrough
	case "november":
		return Month(11)
	case "dec":
		fallthrough
	case "december":
		return Month(12)
	}
	return Month(-1)
}
