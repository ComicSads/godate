package godate_test

import (
	"testing"
	"gitlab.com/ComicSads/godate"
)

func TestMonth (t *testing.T) {
	cases := []struct{
		Test int
		Want string
	}{
		{1, "January"},
		{4, "April"},
		{9, "September"},
		{6, "June"},
		{12, "December"},
	}
	for _, test := range cases {
		m := godate.Month(test.Test)
		s := m.String()
		if s != test.Want {
			t.Errorf("Got %s, wanted %s", s, test.Want)
		}
	}
}

func TestDatePrint (t *testing.T) {
	cases := []struct{
		Test godate.Date
		Want string
	}{
		{godate.NewDate(2019,7,04), "2019-07-04"},
		{godate.NewDate(2069,4,20), "2069-04-20"},
	}
	for _, test := range cases {
		d := test.Test
		s := d.String()
		if s != test.Want {
			t.Errorf("Got %s, wanted %s", s, test.Want)
		}
	}
}
